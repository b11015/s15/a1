let greet = "Hello World"
console.log(greet);

	let fname = "First Name: Bryan";
	console.log(fname);
	let lname = "Last Name: Ocampo";
	console.log(lname);

	let age1 = "Age: 25";
	console.log(age1);

	let hob = "Hobbies:";
	console.log(hob);

	let hob2 = ["Basketball", "Tennis", "Volleyball"];
	console.log(hob2);

	let add = "Work Address:";
	console.log(add);

	let address = {
		houseNumber: '333',
		street: "Washington",
		city: "Antipolo"
	};
	console.log(address);

	let fullName = "My fullname: Steve Rogers";
	console.log(fullName);

	let age = "My current age is: 25";
	console.log(age);
	
	let friend = "My friends:";
	console.log(friend);

	let friends = ["Tony", "Bruce", "Thor", "Natasha", "Clint", "Nick"];
	console.log(friends);

	 let profile = {
	 	username: "captain_america",
	 	fullName: "Steve Rogers",
	 	age: 40,
	 	isActive: false,
	 };
	 console.log(profile);
	 

	 let fullName1 = "Bucky Barnes";
	 console.log(fullName1);

	 const lastLocation = "Arctic Ocean";
	 console.log(lastLocation);
